data:extend({
  -- Black Concrete
  {
    type = "item",
    name = "concrete-black",
    icon = "__ColouredConcrete__/graphics/concrete-black/icon.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "terrain",
    order = "b[concrete]-a[plain]",
    stack_size = 100,
    place_as_tile =
    {
      result = "concrete-black",
      condition_size = 4,
      condition = { "water-tile" }
    }
  },
  -- Dark Blue Concrete
  {
    type = "item",
    name = "concrete-dark-blue",
    icon = "__ColouredConcrete__/graphics/concrete-dark-blue/icon.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "terrain",
    order = "b[concrete-dark-blue]-a[plain]",
    stack_size = 100,
    place_as_tile =
    {
      result = "concrete-dark-blue",
      condition_size = 4,
      condition = { "water-tile" }
    }
  },
  -- Dark Green Concrete
  {
    type = "item",
    name = "concrete-dark-green",
    icon = "__ColouredConcrete__/graphics/concrete-dark-green/icon.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "terrain",
    order = "b[concrete-dark-green]-a[plain]",
    stack_size = 100,
    place_as_tile =
    {
      result = "concrete-dark-green",
      condition_size = 4,
      condition = { "water-tile" }
    }
  },
  -- Dark Aqua Concrete
  {
    type = "item",
    name = "concrete-dark-aqua",
    icon = "__ColouredConcrete__/graphics/concrete-dark-aqua/icon.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "terrain",
    order = "b[concrete-dark-aqua]-a[plain]",
    stack_size = 100,
    place_as_tile =
    {
      result = "concrete-dark-aqua",
      condition_size = 4,
      condition = { "water-tile" }
    }
  },
  -- Dark Red Concrete
  {
    type = "item",
    name = "concrete-dark-red",
    icon = "__ColouredConcrete__/graphics/concrete-dark-red/icon.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "terrain",
    order = "b[concrete-dark-red]-a[plain]",
    stack_size = 100,
    place_as_tile =
    {
      result = "concrete-dark-red",
      condition_size = 4,
      condition = { "water-tile" }
    }
  },
  -- Dark Purple Concrete
  {
    type = "item",
    name = "concrete-dark-purple",
    icon = "__ColouredConcrete__/graphics/concrete-dark-purple/icon.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "terrain",
    order = "b[concrete-dark-purple]-a[plain]",
    stack_size = 100,
    place_as_tile =
    {
      result = "concrete-dark-purple",
      condition_size = 4,
      condition = { "water-tile" }
    }
  },
  -- Gold Concrete
  {
    type = "item",
    name = "concrete-gold",
    icon = "__ColouredConcrete__/graphics/concrete-gold/icon.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "terrain",
    order = "b[concrete-gold]-a[plain]",
    stack_size = 100,
    place_as_tile =
    {
      result = "concrete-gold",
      condition_size = 4,
      condition = { "water-tile" }
    }
  },
  -- Dark Gray Concrete
  {
    type = "item",
    name = "concrete-dark-gray",
    icon = "__ColouredConcrete__/graphics/concrete-dark-gray/icon.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "terrain",
    order = "b[concrete-dark-gray]-a[plain]",
    stack_size = 100,
    place_as_tile =
    {
      result = "concrete-dark-gray",
      condition_size = 4,
      condition = { "water-tile" }
    }
  },
  -- Blue Concrete
  {
    type = "item",
    name = "concrete-blue",
    icon = "__ColouredConcrete__/graphics/concrete-blue/icon.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "terrain",
    order = "b[concrete-blue]-a[plain]",
    stack_size = 100,
    place_as_tile =
    {
      result = "concrete-blue",
      condition_size = 4,
      condition = { "water-tile" }
    }
  },
  -- Green Concrete
  {
    type = "item",
    name = "concrete-green",
    icon = "__ColouredConcrete__/graphics/concrete-green/icon.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "terrain",
    order = "b[concrete-green]-a[plain]",
    stack_size = 100,
    place_as_tile =
    {
      result = "concrete-green",
      condition_size = 4,
      condition = { "water-tile" }
    }
  },
  -- Aqua Concrete
  {
    type = "item",
    name = "concrete-aqua",
    icon = "__ColouredConcrete__/graphics/concrete-aqua/icon.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "terrain",
    order = "b[concrete-aqua]-a[plain]",
    stack_size = 100,
    place_as_tile =
    {
      result = "concrete-aqua",
      condition_size = 4,
      condition = { "water-tile" }
    }
  },
  -- Red Concrete
  {
    type = "item",
    name = "concrete-red",
    icon = "__ColouredConcrete__/graphics/concrete-red/icon.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "terrain",
    order = "b[concrete-red]-a[plain]",
    stack_size = 100,
    place_as_tile =
    {
      result = "concrete-red",
      condition_size = 4,
      condition = { "water-tile" }
    }
  },
  -- Light Purple Concrete
  {
    type = "item",
    name = "concrete-light-purple",
    icon = "__ColouredConcrete__/graphics/concrete-light-purple/icon.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "terrain",
    order = "b[concrete-light-purple]-a[plain]",
    stack_size = 100,
    place_as_tile =
    {
      result = "concrete-light-purple",
      condition_size = 4,
      condition = { "water-tile" }
    }
  },
  -- Yellow Concrete
  {
    type = "item",
    name = "concrete-yellow",
    icon = "__ColouredConcrete__/graphics/concrete-yellow/icon.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "terrain",
    order = "b[concrete-yellow]-a[plain]",
    stack_size = 100,
    place_as_tile =
    {
      result = "concrete-yellow",
      condition_size = 4,
      condition = { "water-tile" }
    }
  },
  -- White Concrete
  {
    type = "item",
    name = "concrete-white",
    icon = "__ColouredConcrete__/graphics/concrete-white/icon.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "terrain",
    order = "b[concrete-white]-a[plain]",
    stack_size = 100,
    place_as_tile =
    {
      result = "concrete-white",
      condition_size = 4,
      condition = { "water-tile" }
    }
  },
})
