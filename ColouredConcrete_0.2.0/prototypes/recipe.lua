data:extend({
  -- Black Concrete
 {
    type = "recipe",
    name = "concrete-black",
    enabled = "true",
    ingredients =
    {
      {"concrete",10}
    },
    result = "concrete-black",
    result_count = 10
  },
  -- Dark Blue Concrete
 {
    type = "recipe",
    name = "concrete-dark-blue",
    enabled = "true",
    ingredients =
    {
      {"concrete",10}
    },
    result = "concrete-dark-blue",
    result_count = 10
  },
  -- Dark Green Concrete
 {
    type = "recipe",
    name = "concrete-dark-green",
    enabled = "true",
    ingredients =
    {
      {"concrete",10}
    },
    result = "concrete-dark-green",
    result_count = 10
  },
  -- Dark Aqua Concrete
 {
    type = "recipe",
    name = "concrete-dark-aqua",
    enabled = "true",
    ingredients =
    {
      {"concrete",10}
    },
    result = "concrete-dark-aqua",
    result_count = 10
  },
  -- Dark Red Concrete
 {
    type = "recipe",
    name = "concrete-dark-red",
    enabled = "true",
    ingredients =
    {
      {"concrete",10}
    },
    result = "concrete-dark-red",
    result_count = 10
  },
  -- Dark Purple Concrete
 {
    type = "recipe",
    name = "concrete-dark-purple",
    enabled = "true",
    ingredients =
    {
      {"concrete",10}
    },
    result = "concrete-dark-purple",
    result_count = 10
  },
  -- Gold Concrete
 {
    type = "recipe",
    name = "concrete-gold",
    enabled = "true",
    ingredients =
    {
      {"concrete",10}
    },
    result = "concrete-gold",
    result_count = 10
  },
  -- Dark Gray Concrete
 {
    type = "recipe",
    name = "concrete-dark-gray",
    enabled = "true",
    ingredients =
    {
      {"concrete",10}
    },
    result = "concrete-dark-gray",
    result_count = 10
  },
  -- Blue Concrete
 {
    type = "recipe",
    name = "concrete-blue",
    enabled = "true",
    ingredients =
    {
      {"concrete",10}
    },
    result = "concrete-blue",
    result_count = 10
  },
  -- Green Concrete
 {
    type = "recipe",
    name = "concrete-green",
    enabled = "true",
    ingredients =
    {
      {"concrete",10}
    },
    result = "concrete-green",
    result_count = 10
  },
  -- Aqua Concrete
 {
    type = "recipe",
    name = "concrete-aqua",
    enabled = "true",
    ingredients =
    {
      {"concrete",10}
    },
    result = "concrete-aqua",
    result_count = 10
  },
  -- Red Concrete
 {
    type = "recipe",
    name = "concrete-red",
    enabled = "true",
    ingredients =
    {
      {"concrete",10}
    },
    result = "concrete-red",
    result_count = 10
  },
  -- Light Purple Concrete
 {
    type = "recipe",
    name = "concrete-light-purple",
    enabled = "true",
    ingredients =
    {
      {"concrete",10}
    },
    result = "concrete-light-purple",
    result_count = 10
  },
  -- Yellow Concrete
 {
    type = "recipe",
    name = "concrete-yellow",
    enabled = "true",
    ingredients =
    {
      {"concrete",10}
    },
    result = "concrete-yellow",
    result_count = 10
  },
  -- White Concrete
 {
    type = "recipe",
    name = "concrete-white",
    enabled = "true",
    ingredients =
    {
      {"concrete",10}
    },
    result = "concrete-white",
    result_count = 10
  },

})
