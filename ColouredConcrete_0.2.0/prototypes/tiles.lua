data:extend({
  -- Black Concrete
  {
    type = "tile",
    name = "concrete-black",
    needs_correction = false,
    minable = {hardness = 0.2, mining_time = 0.5, result = "concrete"},
    mined_sound = { filename = "__base__/sound/deconstruct-bricks.ogg" },
    collision_mask = {"ground-tile"},
    walking_speed_modifier = 1.4,
    layer = 61,
    decorative_removal_probability = 0.9,
    variants =
    {
      main =
      {
        {
          picture = "__ColouredConcrete__/graphics/concrete-black/concrete1.png",
          count = 16,
          size = 1
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-black/concrete2.png",
          count = 4,
          size = 2,
          probability = 0.39,
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-black/concrete4.png",
          count = 4,
          size = 4,
          probability = 1,
        },
      },
      inner_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-black/concrete-inner-corner.png",
        count = 8
      },
      outer_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-black/concrete-outer-corner.png",
        count = 8
      },
      side =
      {
        picture = "__ColouredConcrete__/graphics/concrete-black/concrete-side.png",
        count = 8
      },
      u_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-black/concrete-u.png",
        count = 8
      },
      o_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-black/concrete-o.png",
        count = 1
      }
    },
    walking_sound =
    {
      {
        filename = "__base__/sound/walking/concrete-01.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-02.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-03.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-04.ogg",
        volume = 1.2
      }
    },
    map_color={r=0, g=0, b=0},
    ageing=0,
    vehicle_friction_modifier = concrete_vehicle_speed_modifier
  },
  -- Dark Blue Concrete
  {
    type = "tile",
    name = "concrete-dark-blue",
    needs_correction = false,
    minable = {hardness = 0.2, mining_time = 0.5, result = "concrete"},
    mined_sound = { filename = "__base__/sound/deconstruct-bricks.ogg" },
    collision_mask = {"ground-tile"},
    walking_speed_modifier = 1.4,
    layer = 61,
    decorative_removal_probability = 0.9,
    variants =
    {
      main =
      {
        {
          picture = "__ColouredConcrete__/graphics/concrete-dark-blue/concrete1.png",
          count = 16,
          size = 1
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-dark-blue/concrete2.png",
          count = 4,
          size = 2,
          probability = 0.39,
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-dark-blue/concrete4.png",
          count = 4,
          size = 4,
          probability = 1,
        },
      },
      inner_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-blue/concrete-inner-corner.png",
        count = 8
      },
      outer_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-blue/concrete-outer-corner.png",
        count = 8
      },
      side =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-blue/concrete-side.png",
        count = 8
      },
      u_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-blue/concrete-u.png",
        count = 8
      },
      o_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-blue/concrete-o.png",
        count = 1
      }
    },
    walking_sound =
    {
      {
        filename = "__base__/sound/walking/concrete-01.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-02.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-03.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-04.ogg",
        volume = 1.2
      }
    },
    map_color={r=0, g=0, b=170},
    ageing=0,
    vehicle_friction_modifier = concrete_vehicle_speed_modifier
  },
  -- Dark Green Concrete
  {
    type = "tile",
    name = "concrete-dark-green",
    needs_correction = false,
    minable = {hardness = 0.2, mining_time = 0.5, result = "concrete"},
    mined_sound = { filename = "__base__/sound/deconstruct-bricks.ogg" },
    collision_mask = {"ground-tile"},
    walking_speed_modifier = 1.4,
    layer = 61,
    decorative_removal_probability = 0.9,
    variants =
    {
      main =
      {
        {
          picture = "__ColouredConcrete__/graphics/concrete-dark-green/concrete1.png",
          count = 16,
          size = 1
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-dark-green/concrete2.png",
          count = 4,
          size = 2,
          probability = 0.39,
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-dark-green/concrete4.png",
          count = 4,
          size = 4,
          probability = 1,
        },
      },
      inner_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-green/concrete-inner-corner.png",
        count = 8
      },
      outer_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-green/concrete-outer-corner.png",
        count = 8
      },
      side =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-green/concrete-side.png",
        count = 8
      },
      u_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-green/concrete-u.png",
        count = 8
      },
      o_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-green/concrete-o.png",
        count = 1
      }
    },
    walking_sound =
    {
      {
        filename = "__base__/sound/walking/concrete-01.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-02.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-03.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-04.ogg",
        volume = 1.2
      }
    },
    map_color={r=0, g=170, b=0},
    ageing=0,
    vehicle_friction_modifier = concrete_vehicle_speed_modifier
  },
  -- Dark Aqua Concrete
  {
    type = "tile",
    name = "concrete-dark-aqua",
    needs_correction = false,
    minable = {hardness = 0.2, mining_time = 0.5, result = "concrete"},
    mined_sound = { filename = "__base__/sound/deconstruct-bricks.ogg" },
    collision_mask = {"ground-tile"},
    walking_speed_modifier = 1.4,
    layer = 61,
    decorative_removal_probability = 0.9,
    variants =
    {
      main =
      {
        {
          picture = "__ColouredConcrete__/graphics/concrete-dark-aqua/concrete1.png",
          count = 16,
          size = 1
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-dark-aqua/concrete2.png",
          count = 4,
          size = 2,
          probability = 0.39,
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-dark-aqua/concrete4.png",
          count = 4,
          size = 4,
          probability = 1,
        },
      },
      inner_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-aqua/concrete-inner-corner.png",
        count = 8
      },
      outer_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-aqua/concrete-outer-corner.png",
        count = 8
      },
      side =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-aqua/concrete-side.png",
        count = 8
      },
      u_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-aqua/concrete-u.png",
        count = 8
      },
      o_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-aqua/concrete-o.png",
        count = 1
      }
    },
    walking_sound =
    {
      {
        filename = "__base__/sound/walking/concrete-01.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-02.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-03.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-04.ogg",
        volume = 1.2
      }
    },
    map_color={r=0, g=170, b=170},
    ageing=0,
    vehicle_friction_modifier = concrete_vehicle_speed_modifier
  },
  -- Dark Red Concrete
  {
    type = "tile",
    name = "concrete-dark-red",
    needs_correction = false,
    minable = {hardness = 0.2, mining_time = 0.5, result = "concrete"},
    mined_sound = { filename = "__base__/sound/deconstruct-bricks.ogg" },
    collision_mask = {"ground-tile"},
    walking_speed_modifier = 1.4,
    layer = 61,
    decorative_removal_probability = 0.9,
    variants =
    {
      main =
      {
        {
          picture = "__ColouredConcrete__/graphics/concrete-dark-red/concrete1.png",
          count = 16,
          size = 1
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-dark-red/concrete2.png",
          count = 4,
          size = 2,
          probability = 0.39,
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-dark-red/concrete4.png",
          count = 4,
          size = 4,
          probability = 1,
        },
      },
      inner_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-red/concrete-inner-corner.png",
        count = 8
      },
      outer_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-red/concrete-outer-corner.png",
        count = 8
      },
      side =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-red/concrete-side.png",
        count = 8
      },
      u_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-red/concrete-u.png",
        count = 8
      },
      o_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-red/concrete-o.png",
        count = 1
      }
    },
    walking_sound =
    {
      {
        filename = "__base__/sound/walking/concrete-01.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-02.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-03.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-04.ogg",
        volume = 1.2
      }
    },
    map_color={r=170, g=0, b=0},
    ageing=0,
    vehicle_friction_modifier = concrete_vehicle_speed_modifier
  },
  -- Dark Purple Concrete
  {
    type = "tile",
    name = "concrete-dark-purple",
    needs_correction = false,
    minable = {hardness = 0.2, mining_time = 0.5, result = "concrete"},
    mined_sound = { filename = "__base__/sound/deconstruct-bricks.ogg" },
    collision_mask = {"ground-tile"},
    walking_speed_modifier = 1.4,
    layer = 61,
    decorative_removal_probability = 0.9,
    variants =
    {
      main =
      {
        {
          picture = "__ColouredConcrete__/graphics/concrete-dark-purple/concrete1.png",
          count = 16,
          size = 1
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-dark-purple/concrete2.png",
          count = 4,
          size = 2,
          probability = 0.39,
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-dark-purple/concrete4.png",
          count = 4,
          size = 4,
          probability = 1,
        },
      },
      inner_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-purple/concrete-inner-corner.png",
        count = 8
      },
      outer_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-purple/concrete-outer-corner.png",
        count = 8
      },
      side =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-purple/concrete-side.png",
        count = 8
      },
      u_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-purple/concrete-u.png",
        count = 8
      },
      o_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-purple/concrete-o.png",
        count = 1
      }
    },
    walking_sound =
    {
      {
        filename = "__base__/sound/walking/concrete-01.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-02.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-03.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-04.ogg",
        volume = 1.2
      }
    },
    map_color={r=170, g=0, b=170},
    ageing=0,
    vehicle_friction_modifier = concrete_vehicle_speed_modifier
  },
  -- Gold Concrete
  {
    type = "tile",
    name = "concrete-gold",
    needs_correction = false,
    minable = {hardness = 0.2, mining_time = 0.5, result = "concrete"},
    mined_sound = { filename = "__base__/sound/deconstruct-bricks.ogg" },
    collision_mask = {"ground-tile"},
    walking_speed_modifier = 1.4,
    layer = 61,
    decorative_removal_probability = 0.9,
    variants =
    {
      main =
      {
        {
          picture = "__ColouredConcrete__/graphics/concrete-gold/concrete1.png",
          count = 16,
          size = 1
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-gold/concrete2.png",
          count = 4,
          size = 2,
          probability = 0.39,
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-gold/concrete4.png",
          count = 4,
          size = 4,
          probability = 1,
        },
      },
      inner_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-gold/concrete-inner-corner.png",
        count = 8
      },
      outer_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-gold/concrete-outer-corner.png",
        count = 8
      },
      side =
      {
        picture = "__ColouredConcrete__/graphics/concrete-gold/concrete-side.png",
        count = 8
      },
      u_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-gold/concrete-u.png",
        count = 8
      },
      o_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-gold/concrete-o.png",
        count = 1
      }
    },
    walking_sound =
    {
      {
        filename = "__base__/sound/walking/concrete-01.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-02.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-03.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-04.ogg",
        volume = 1.2
      }
    },
    map_color={r=255, g=170, b=0},
    ageing=0,
    vehicle_friction_modifier = concrete_vehicle_speed_modifier
  },
  -- Dark Gray Concrete
  {
    type = "tile",
    name = "concrete-dark-gray",
    needs_correction = false,
    minable = {hardness = 0.2, mining_time = 0.5, result = "concrete"},
    mined_sound = { filename = "__base__/sound/deconstruct-bricks.ogg" },
    collision_mask = {"ground-tile"},
    walking_speed_modifier = 1.4,
    layer = 61,
    decorative_removal_probability = 0.9,
    variants =
    {
      main =
      {
        {
          picture = "__ColouredConcrete__/graphics/concrete-dark-gray/concrete1.png",
          count = 16,
          size = 1
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-dark-gray/concrete2.png",
          count = 4,
          size = 2,
          probability = 0.39,
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-dark-gray/concrete4.png",
          count = 4,
          size = 4,
          probability = 1,
        },
      },
      inner_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-gray/concrete-inner-corner.png",
        count = 8
      },
      outer_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-gray/concrete-outer-corner.png",
        count = 8
      },
      side =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-gray/concrete-side.png",
        count = 8
      },
      u_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-gray/concrete-u.png",
        count = 8
      },
      o_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-dark-gray/concrete-o.png",
        count = 1
      }
    },
    walking_sound =
    {
      {
        filename = "__base__/sound/walking/concrete-01.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-02.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-03.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-04.ogg",
        volume = 1.2
      }
    },
    map_color={r=85, g=85, b=85},
    ageing=0,
    vehicle_friction_modifier = concrete_vehicle_speed_modifier
  },
  -- Blue Concrete
  {
    type = "tile",
    name = "concrete-blue",
    needs_correction = false,
    minable = {hardness = 0.2, mining_time = 0.5, result = "concrete"},
    mined_sound = { filename = "__base__/sound/deconstruct-bricks.ogg" },
    collision_mask = {"ground-tile"},
    walking_speed_modifier = 1.4,
    layer = 61,
    decorative_removal_probability = 0.9,
    variants =
    {
      main =
      {
        {
          picture = "__ColouredConcrete__/graphics/concrete-blue/concrete1.png",
          count = 16,
          size = 1
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-blue/concrete2.png",
          count = 4,
          size = 2,
          probability = 0.39,
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-blue/concrete4.png",
          count = 4,
          size = 4,
          probability = 1,
        },
      },
      inner_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-blue/concrete-inner-corner.png",
        count = 8
      },
      outer_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-blue/concrete-outer-corner.png",
        count = 8
      },
      side =
      {
        picture = "__ColouredConcrete__/graphics/concrete-blue/concrete-side.png",
        count = 8
      },
      u_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-blue/concrete-u.png",
        count = 8
      },
      o_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-blue/concrete-o.png",
        count = 1
      }
    },
    walking_sound =
    {
      {
        filename = "__base__/sound/walking/concrete-01.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-02.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-03.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-04.ogg",
        volume = 1.2
      }
    },
    map_color={r=85, g=85, b=255},
    ageing=0,
    vehicle_friction_modifier = concrete_vehicle_speed_modifier
  },
  -- Green Concrete
  {
    type = "tile",
    name = "concrete-green",
    needs_correction = false,
    minable = {hardness = 0.2, mining_time = 0.5, result = "concrete"},
    mined_sound = { filename = "__base__/sound/deconstruct-bricks.ogg" },
    collision_mask = {"ground-tile"},
    walking_speed_modifier = 1.4,
    layer = 61,
    decorative_removal_probability = 0.9,
    variants =
    {
      main =
      {
        {
          picture = "__ColouredConcrete__/graphics/concrete-green/concrete1.png",
          count = 16,
          size = 1
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-green/concrete2.png",
          count = 4,
          size = 2,
          probability = 0.39,
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-green/concrete4.png",
          count = 4,
          size = 4,
          probability = 1,
        },
      },
      inner_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-green/concrete-inner-corner.png",
        count = 8
      },
      outer_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-green/concrete-outer-corner.png",
        count = 8
      },
      side =
      {
        picture = "__ColouredConcrete__/graphics/concrete-green/concrete-side.png",
        count = 8
      },
      u_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-green/concrete-u.png",
        count = 8
      },
      o_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-green/concrete-o.png",
        count = 1
      }
    },
    walking_sound =
    {
      {
        filename = "__base__/sound/walking/concrete-01.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-02.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-03.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-04.ogg",
        volume = 1.2
      }
    },
    map_color={r=85, g=255, b=85},
    ageing=0,
    vehicle_friction_modifier = concrete_vehicle_speed_modifier
  },
  -- Aqua Concrete
  {
    type = "tile",
    name = "concrete-aqua",
    needs_correction = false,
    minable = {hardness = 0.2, mining_time = 0.5, result = "concrete"},
    mined_sound = { filename = "__base__/sound/deconstruct-bricks.ogg" },
    collision_mask = {"ground-tile"},
    walking_speed_modifier = 1.4,
    layer = 61,
    decorative_removal_probability = 0.9,
    variants =
    {
      main =
      {
        {
          picture = "__ColouredConcrete__/graphics/concrete-aqua/concrete1.png",
          count = 16,
          size = 1
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-aqua/concrete2.png",
          count = 4,
          size = 2,
          probability = 0.39,
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-aqua/concrete4.png",
          count = 4,
          size = 4,
          probability = 1,
        },
      },
      inner_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-aqua/concrete-inner-corner.png",
        count = 8
      },
      outer_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-aqua/concrete-outer-corner.png",
        count = 8
      },
      side =
      {
        picture = "__ColouredConcrete__/graphics/concrete-aqua/concrete-side.png",
        count = 8
      },
      u_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-aqua/concrete-u.png",
        count = 8
      },
      o_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-aqua/concrete-o.png",
        count = 1
      }
    },
    walking_sound =
    {
      {
        filename = "__base__/sound/walking/concrete-01.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-02.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-03.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-04.ogg",
        volume = 1.2
      }
    },
    map_color={r=170, g=0, b=0},
    ageing=0,
    vehicle_friction_modifier = concrete_vehicle_speed_modifier
  },
  -- Red Concrete
  {
    type = "tile",
    name = "concrete-red",
    needs_correction = false,
    minable = {hardness = 0.2, mining_time = 0.5, result = "concrete"},
    mined_sound = { filename = "__base__/sound/deconstruct-bricks.ogg" },
    collision_mask = {"ground-tile"},
    walking_speed_modifier = 1.4,
    layer = 61,
    decorative_removal_probability = 0.9,
    variants =
    {
      main =
      {
        {
          picture = "__ColouredConcrete__/graphics/concrete-red/concrete1.png",
          count = 16,
          size = 1
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-red/concrete2.png",
          count = 4,
          size = 2,
          probability = 0.39,
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-red/concrete4.png",
          count = 4,
          size = 4,
          probability = 1,
        },
      },
      inner_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-red/concrete-inner-corner.png",
        count = 8
      },
      outer_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-red/concrete-outer-corner.png",
        count = 8
      },
      side =
      {
        picture = "__ColouredConcrete__/graphics/concrete-red/concrete-side.png",
        count = 8
      },
      u_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-red/concrete-u.png",
        count = 8
      },
      o_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-red/concrete-o.png",
        count = 1
      }
    },
    walking_sound =
    {
      {
        filename = "__base__/sound/walking/concrete-01.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-02.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-03.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-04.ogg",
        volume = 1.2
      }
    },
    map_color={r=170, g=0, b=0},
    ageing=0,
    vehicle_friction_modifier = concrete_vehicle_speed_modifier
  },
  -- Light Purple Concrete
  {
    type = "tile",
    name = "concrete-light-purple",
    needs_correction = false,
    minable = {hardness = 0.2, mining_time = 0.5, result = "concrete"},
    mined_sound = { filename = "__base__/sound/deconstruct-bricks.ogg" },
    collision_mask = {"ground-tile"},
    walking_speed_modifier = 1.4,
    layer = 61,
    decorative_removal_probability = 0.9,
    variants =
    {
      main =
      {
        {
          picture = "__ColouredConcrete__/graphics/concrete-light-purple/concrete1.png",
          count = 16,
          size = 1
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-light-purple/concrete2.png",
          count = 4,
          size = 2,
          probability = 0.39,
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-light-purple/concrete4.png",
          count = 4,
          size = 4,
          probability = 1,
        },
      },
      inner_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-light-purple/concrete-inner-corner.png",
        count = 8
      },
      outer_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-light-purple/concrete-outer-corner.png",
        count = 8
      },
      side =
      {
        picture = "__ColouredConcrete__/graphics/concrete-light-purple/concrete-side.png",
        count = 8
      },
      u_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-light-purple/concrete-u.png",
        count = 8
      },
      o_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-light-purple/concrete-o.png",
        count = 1
      }
    },
    walking_sound =
    {
      {
        filename = "__base__/sound/walking/concrete-01.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-02.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-03.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-04.ogg",
        volume = 1.2
      }
    },
    map_color={r=170, g=0, b=0},
    ageing=0,
    vehicle_friction_modifier = concrete_vehicle_speed_modifier
  },
  -- Yellow Concrete
  {
    type = "tile",
    name = "concrete-yellow",
    needs_correction = false,
    minable = {hardness = 0.2, mining_time = 0.5, result = "concrete"},
    mined_sound = { filename = "__base__/sound/deconstruct-bricks.ogg" },
    collision_mask = {"ground-tile"},
    walking_speed_modifier = 1.4,
    layer = 61,
    decorative_removal_probability = 0.9,
    variants =
    {
      main =
      {
        {
          picture = "__ColouredConcrete__/graphics/concrete-yellow/concrete1.png",
          count = 16,
          size = 1
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-yellow/concrete2.png",
          count = 4,
          size = 2,
          probability = 0.39,
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-yellow/concrete4.png",
          count = 4,
          size = 4,
          probability = 1,
        },
      },
      inner_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-yellow/concrete-inner-corner.png",
        count = 8
      },
      outer_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-yellow/concrete-outer-corner.png",
        count = 8
      },
      side =
      {
        picture = "__ColouredConcrete__/graphics/concrete-yellow/concrete-side.png",
        count = 8
      },
      u_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-yellow/concrete-u.png",
        count = 8
      },
      o_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-yellow/concrete-o.png",
        count = 1
      }
    },
    walking_sound =
    {
      {
        filename = "__base__/sound/walking/concrete-01.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-02.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-03.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-04.ogg",
        volume = 1.2
      }
    },
    map_color={r=170, g=0, b=0},
    ageing=0,
    vehicle_friction_modifier = concrete_vehicle_speed_modifier
  },
  -- White Concrete
  {
    type = "tile",
    name = "concrete-white",
    needs_correction = false,
    minable = {hardness = 0.2, mining_time = 0.5, result = "concrete"},
    mined_sound = { filename = "__base__/sound/deconstruct-bricks.ogg" },
    collision_mask = {"ground-tile"},
    walking_speed_modifier = 1.4,
    layer = 61,
    decorative_removal_probability = 0.9,
    variants =
    {
      main =
      {
        {
          picture = "__ColouredConcrete__/graphics/concrete-white/concrete1.png",
          count = 16,
          size = 1
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-white/concrete2.png",
          count = 4,
          size = 2,
          probability = 0.39,
        },
        {
          picture = "__ColouredConcrete__/graphics/concrete-white/concrete4.png",
          count = 4,
          size = 4,
          probability = 1,
        },
      },
      inner_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-white/concrete-inner-corner.png",
        count = 8
      },
      outer_corner =
      {
        picture = "__ColouredConcrete__/graphics/concrete-white/concrete-outer-corner.png",
        count = 8
      },
      side =
      {
        picture = "__ColouredConcrete__/graphics/concrete-white/concrete-side.png",
        count = 8
      },
      u_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-white/concrete-u.png",
        count = 8
      },
      o_transition =
      {
        picture = "__ColouredConcrete__/graphics/concrete-white/concrete-o.png",
        count = 1
      }
    },
    walking_sound =
    {
      {
        filename = "__base__/sound/walking/concrete-01.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-02.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-03.ogg",
        volume = 1.2
      },
      {
        filename = "__base__/sound/walking/concrete-04.ogg",
        volume = 1.2
      }
    },
    map_color={r=255, g=255, b=255},
    ageing=0,
    vehicle_friction_modifier = concrete_vehicle_speed_modifier
  },
})
