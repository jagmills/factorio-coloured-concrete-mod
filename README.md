# README #

This is a mod for Factorio (0.13.x) which adds 15 additional colours of concrete tile into the game, allowing for art, signs and various other creative uses.

### About ###

The colours are reproduced from the Minecraft 16-colour set, and are named as such (normal concrete being 'gray').

Any of the coloured concrete colours can be crafted simply from one piece of normal concrete.

### To Do: ###
Add appropriate technology research.